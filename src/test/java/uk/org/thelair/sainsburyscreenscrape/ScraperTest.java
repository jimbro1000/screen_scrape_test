package uk.org.thelair.sainsburyscreenscrape;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;

import static org.hamcrest.CoreMatchers.*;

public class ScraperTest {
    private static final String WEB_TEST_URL = "https://jsainsburyplc.github.io/serverside-test/site/www.sainsburys.co.uk/webapp/wcs/stores/servlet/gb/groceries/berries-cherries-currants6039.html";
    private static final String WEB_TEST_FAILURE_URL = "https://jsainsburyplc.github.iox/serverside-test/site/www.sainsburys.co.uk/webapp/wcs/stores/servlet/gb/groceries/berries-cherries-currants6039.html";
    private static final String PRODUCT_LINK = "https://jsainsburyplc.github.io/serverside-test/site/www.sainsburys.co.uk/shop/gb/groceries/berries-cherries-currants/sainsburys-british-strawberries-400g.html";
    private static final String RELATIVE_URL = "../../../../../../shop/gb/groceries/berries-cherries-currants/sainsburys-british-strawberries-400g.html";

    private final PrintStream stdout = System.out;
    private final ByteArrayOutputStream output = new ByteArrayOutputStream();

    private Scraper scraper;

    @Before
    public void setup() throws UnsupportedEncodingException {
        scraper = new Scraper(WEB_TEST_URL);
        System.setOut(new PrintStream(output, true, "UTF-8"));
    }
    @After
    public void cleanUp() {
        System.setOut(stdout);
    }

    @Test
    public void itHasARunMethodThatExecutesTheScrape() throws Exception {
        scraper.run();
        ObjectMapper objectMapper = new ObjectMapper();
        Products products = objectMapper.readValue(output.toString(), Products.class);
        Assert.assertThat("products should not be null", products, is(notNullValue()));
        Assert.assertThat("products should contain 17 items", products.getProducts().length, is(equalTo(17)));
        Assert.assertThat("products total should be 39.50", products.getTotal(), is(equalTo(39.5f)));
    }
    @Test
    public void itShouldCloseGracefullyIfTheTargetUrlCannotBeContacted() {
        scraper = new Scraper(WEB_TEST_FAILURE_URL);
        scraper.run();
        Assert.assertThat("it should report that it can't contact the target page", output.toString(), containsString("Failed to capture URL"));
    }
    @Test(expected = IllegalArgumentException.class)
    public void scraperHasAMethodCaptureToGrabAWebPageAndReturnsAnIllegalArgumentExceptionIfUrlIsInvalid() throws Exception {
        scraper.capture("url");
    }
    @Test
    public void itHasAConsoleReadyMainMethod() throws Exception {
        Scraper.main(new String[0]);
        ObjectMapper objectMapper = new ObjectMapper();
        Products products = objectMapper.readValue(output.toString(), Products.class);
        Assert.assertThat("products should not be null", products, is(notNullValue()));
        Assert.assertThat("products should contain 17 items", products.getProducts().length, is(equalTo(17)));
        Assert.assertThat("products total should be 39.50", products.getTotal(), is(equalTo(39.5f)));
    }
    @Test
    public void captureMethodReturnsAValidJsoupDocumentIfTheUrlExists() throws Exception {
        Document document = scraper.capture("http://www.google.com");
        Assert.assertThat("Expected a Jsoup Document", document, is(notNullValue()));
    }
    @Test
    public void scraperHasAMethodScanProductListerThatFindsTheProductsAndReturnsEachInAnArray() throws Exception {
        Document document = scraper.capture(WEB_TEST_URL);
        Element[] elements = scraper.scanProductLister(document);
        Assert.assertThat("scanProductLister finds seventeen different products", elements.length, is(equalTo(17)));
    }
    @Test
    public void scraperHasAMethodScrapeProductThatBuildsAProductPojo() {
        Product product = scraper.scrapeProduct(provideProductElement());
        Assert.assertThat("scrapeProduct returns a valid product pojo", product, is(notNullValue()));
        Assert.assertThat("scraped product has a title of Sainsbury's Strawberries 400g", product.getTitle(), is(equalTo("Sainsbury's Strawberries 400g")));
        Assert.assertThat("scraped product has a unit price of 1.75", product.getUnitPrice(), is(equalTo(1.75f)));
    }
    @Test
    public void scraperHasAMethodFollowProductThatFollowsTheProductLinkAndFindsDescriptionAndNutritionInfoIfItExists() {
        Product product = new Product("Strawberries", "", 1.75f, null);
        product = scraper.followLink(product, PRODUCT_LINK);
        Assert.assertThat("scraped description is 'by Sainsbury's strawberries'", product.getDescription(), is(equalTo("by Sainsbury's strawberries")));
        Assert.assertThat("scraped calories is 33", product.getKCalPerHundredGramme(), is(equalTo(33)));
    }
    @Test
    public void followLinkOnlyFindsTheFirstLineOfTheDescription() {
        Product product = new Product("Strawberries", "", 1.75f, null);
        product = scraper.followLink(product, "https://jsainsburyplc.github.io/serverside-test/site/www.sainsburys.co.uk/shop/gb/groceries/sainsburys-140ml-klip-lock-storage-set---3pk.html");
        Assert.assertThat("scraped description is only '100% airtight: Yes'", product.getDescription(), is(equalTo("100% airtight: Yes")));
    }
    @Test
    public void followLinkFailsGracefullyIfTheLinkIsBroken() {
        Product product = new Product("Strawberries", "", 1.75f, null);
        product = scraper.followLink(product, "https://doesnt.exist.mock/index.html");
        Assert.assertThat("scraped description is still empty", product.getDescription(), is(equalTo("")));
        Assert.assertThat("it should report that it can't contact the linked page", output.toString(), containsString("Product url https://doesnt.exist.mock/index.html cannot be contacted"));
    }
    @Test
    public void scraperHasAMethodToMergeRelativePaths() {
        String followed = scraper.mergeRelativeUrl(WEB_TEST_URL, RELATIVE_URL);
        Assert.assertThat("relative path aligned with current page", followed, is(equalTo("https://jsainsburyplc.github.io/serverside-test/site/www.sainsburys.co.uk/shop/gb/groceries/berries-cherries-currants/sainsburys-british-strawberries-400g.html")));
    }
    @Test
    public void scraperHasAMethodPojoToJson() {
        Products products = new Products();
        products.addProduct(new Product("title", "description", 20.0f, 52));
        String actual = scraper.pojoToJson(products);
        Assert.assertThat("result contains a kay value pair of total : 20.00", actual, containsString("\"total\" : 20.00"));
        Assert.assertThat("result contains a key value pair of title : title", actual, containsString("\"title\" : \"title\""));
        Assert.assertThat("result contains a key value pair of description : description", actual, containsString("\"description\" : \"description\""));
        Assert.assertThat("result contains a key value pair of unitPrice : 20.00", actual, containsString("\"unit_price\" : 20.00"));
        Assert.assertThat("result contains a key value pair of kcal_per_100g : 52", actual, containsString("\"kcal_per_100g\" : 52"));
    }
    @Test
    public void pojoToJsonShowsTotalWithTwoDecimalPlaces() {
        Products products = new Products();
        products.addProduct(new Product("title", "description", 2001.001f, 52));
        String actual = scraper.pojoToJson(products);
        Assert.assertThat("total result is shown to two decimal places", actual, containsString("\"total\" : 2001.00"));
    }
    @Test
    public void pojoToJsonShowsUnitPriceWithTwoDecimalPlaces() {
        Products products = new Products();
        products.addProduct(new Product("title", "description", 2001.001f, 52));
        String actual = scraper.pojoToJson(products);
        Assert.assertThat("unit price result is shown to two decimal places", actual, containsString("\"unit_price\" : 2001.00"));
    }
    @Test
    public void pojoToJsonRoundsAnyPriceToNearestValue() {
        Products products = new Products();
        products.addProduct(new Product("title", "description", 2001.009f, 52));
        String actual = scraper.pojoToJson(products);
        Assert.assertThat("total result is shown rounded up", actual, containsString("\"total\" : 2001.01"));
    }

    private Element provideProductElement() {
        Element result = null;
        try {
            Document document = scraper.capture(WEB_TEST_URL);
            Element[] elements = scraper.scanProductLister(document);
            result = elements[0];
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return result;
    }
}
