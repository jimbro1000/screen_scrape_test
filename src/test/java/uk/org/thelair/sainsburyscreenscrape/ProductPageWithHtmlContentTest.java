package uk.org.thelair.sainsburyscreenscrape;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;

public class ProductPageWithHtmlContentTest {
    private static final String POSITIVE_URL = "https://jsainsburyplc.github.io/serverside-test/site/www.sainsburys.co.uk/shop/gb/groceries/berries-cherries-currants/sainsburys-british-strawberries-400g.html";
    private static final String NEGATIVE_URL = "https://jsainsburyplc.github.io/serverside-test/site/www.sainsburys.co.uk/shop/gb/groceries/sainsburys-140ml-klip-lock-storage-set---3pk.html";
    private static final String ALTERNATIVE_NUTRITION_UTL = "https://jsainsburyplc.github.io/serverside-test/site/www.sainsburys.co.uk/shop/gb/groceries/berries-cherries-currants/sainsburys-cherry-punnet-200g-468015-p-44.html";

    private ProductPageWithHtmlContent productPage;
    @Before
    public void setup() {
        productPage = new ProductPageWithHtmlContent();
    }
    @Test
    public void itImplementsProductPage() {
        Assert.assertThat("Product pages should implement ProductPage interface", productPage, is(instanceOf(ProductPage.class)));
    }
    @Test
    public void identifiesItselfWhenASuitableDomIsSupplied() throws Exception {
        Assert.assertThat("Page should identified positively on a match", productPage.identifyPage(providePositiveDocument()), is(true));
    }
    @Test
    public void returnsAFalseForIdentifyingAgainstANonMatchingDom() throws Exception {
        Document document = Jsoup.connect(NEGATIVE_URL).get();
        Assert.assertThat("Page should rejected on a non-match", productPage.identifyPage(document), is(false));
    }
    @Test
    public void itPopulatesTheMissingDescriptionAndNutritionInTheGivenProduct() throws Exception {
        Product product = new Product("Test", "", 0.0f, null);
        product = productPage.scrapeDetails(providePositiveDocument(), product);
        Assert.assertThat("Description should be populated with Strawberries", product.getDescription(), is(equalTo("by Sainsbury's strawberries")));
    }
    @Test
    public void itHasAMethodExtractCaloriesThatFindsTheCaloriesInformationFromTheProductInfoPage() {
        Assert.assertThat("result is 33", productPage.extractCalories(provideNutritionElement()), is(equalTo(33)));
    }
    @Test
    public void extractCaloriesCanHandleAnAlternativeNutritionTable() {
        Assert.assertThat("result is 52", productPage.extractCalories(provideAlternativeNutritionElement()), is(equalTo(52)));
    }
    @Test(expected = Test.None.class)
    public void scrapeDetailsHandlesAMissingProduct() throws Exception {
        productPage.scrapeDetails(providePositiveDocument(), null);
    }
    @Test(expected = Test.None.class)
    public void scrapeDetailsHandlesAMissingDocument() {
        productPage.scrapeDetails(null, new Product("a", "b", 0.0f, null));
    }
    @Test(expected = Test.None.class)
    public void identifyPageHandlesAMissingDocumentAndReturnsFalse() {
        Assert.assertThat("If the document is missing return false as it cannot be matched", productPage.identifyPage(null), is(false));
    }

    private Document providePositiveDocument() throws Exception {
        return Jsoup.connect(POSITIVE_URL).get();
    }
    private Element provideNutritionElement() {
        Element result = null;
        try {
            Document document = providePositiveDocument();
            Elements elements = document.select("div.productText");
            result = elements.get(1);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return result;
    }
    private Element provideAlternativeNutritionElement() {
        Element result = null;
        try {
            Document document = Jsoup.connect(ALTERNATIVE_NUTRITION_UTL).get();
            Elements elements = document.select("div.productText");
            result = elements.get(1);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return result;
    }
}
