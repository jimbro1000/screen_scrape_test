package uk.org.thelair.sainsburyscreenscrape;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.Assert;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.*;

public class ProductPageFactoryTest {
    private final static String DUMMY_PAGE = "http://www.google.com";
    private final static String PAGE_WITH_HTMLCONTENT = "https://jsainsburyplc.github.io/serverside-test/site/www.sainsburys.co.uk/shop/gb/groceries/berries-cherries-currants/sainsburys-british-strawberries-400g.html";
    private static final String PAGE_WITH_MAINPART = "https://jsainsburyplc.github.io/serverside-test/site/www.sainsburys.co.uk/shop/gb/groceries/sainsburys-140ml-klip-lock-storage-set---3pk.html";

    @Test
    public void itReturnsAProductPageObject() throws Exception {
        Assert.assertThat("factory returns a productPage implementation for HTMLCONTENT", ProductPageFactory.supplyPage(providePage(PAGE_WITH_HTMLCONTENT)), is(instanceOf(ProductPage.class)));
        Assert.assertThat("factory returns a productPage implementation for MAINPART", ProductPageFactory.supplyPage(providePage(PAGE_WITH_MAINPART)), is(instanceOf(ProductPage.class)));
    }
    @Test
    public void itReturnsAProductPageWithHtmlContentObject() throws Exception {
        Assert.assertThat("factory returns a productPage implementation for HTMLCONTENT", ProductPageFactory.supplyPage(providePage(PAGE_WITH_HTMLCONTENT)), is(instanceOf(ProductPageWithHtmlContent.class)));
        Assert.assertThat("factory doesn't return a productPage implementation for MAINPART", ProductPageFactory.supplyPage(providePage(PAGE_WITH_MAINPART)), is(not(instanceOf(ProductPageWithHtmlContent.class))));
    }
    @Test
    public void itReturnsAProductPageWithMainPartObject() throws Exception {
        Assert.assertThat("factory returns a productPage implementation for HTMLCONTENT", ProductPageFactory.supplyPage(providePage(PAGE_WITH_MAINPART)), is(instanceOf(ProductPageWithMainPart.class)));
        Assert.assertThat("factory doesn't return a productPage implementation for MAINPART", ProductPageFactory.supplyPage(providePage(PAGE_WITH_HTMLCONTENT)), is(not(instanceOf(ProductPageWithMainPart.class))));
    }
    @Test
    public void itReturnsNullIfAPageIsNotMatched() throws Exception {
        Assert.assertThat("factory returns a null productPage if no match is found", ProductPageFactory.supplyPage(providePage(DUMMY_PAGE)), is(nullValue()));
    }

    private Document providePage(String url) throws Exception {
        return Jsoup.connect(url).get();
    }
}
