package uk.org.thelair.sainsburyscreenscrape;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;

public class ProductTest {
    private Product product;
    @Before
    public void setup() {
        product = new Product();
    }
    @Test
    public void itHasATitlePropertyThatDefaultsToAnEmptyString() {
        Assert.assertThat("Product has a title property", product.getTitle(), is(equalTo("")));
    }
    @Test
    public void itHasADescriptionPropertyThatDefaultsToAnEmptyString() {
        Assert.assertThat("Product has a description property", product.getDescription(), is(equalTo("")));
    }
    @Test
    public void itHasAUnitPricePropertyThatDefaultsToZero() {
        Assert.assertThat("Product has a unitPrice property", product.getUnitPrice(), is(equalTo(0.0f)));
    }
    @Test
    public void itHasAKCalPerHundredGrammePropertyThatDefaultsToNull() {
        Assert.assertThat("Product has a kCalPerHundredGramme property", product.getKCalPerHundredGramme(), is(nullValue()));
    }
    @Test
    public void itHasAPublicConstructorThatAcceptsTitleDescriptionUnitPriceAndCalories() {
        product = new Product("Title", "Description", 10.4f, 2);
        Assert.assertThat("title is initialised", product.getTitle(), is(equalTo("Title")));
        Assert.assertThat("description is initialised", product.getDescription(), is(equalTo("Description")));
        Assert.assertThat("unitPrice is initialised", product.getUnitPrice(), is(equalTo(10.4f)));
        Assert.assertThat("kCalPerHundredGramme is initialised", product.getKCalPerHundredGramme(), is(equalTo(2)));
    }
}
