package uk.org.thelair.sainsburyscreenscrape;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;

public class ProductsTest {
    private Products products;
    @Before
    public void setup() {
        products = new Products();
    }
    @Test
    public void itHasAProductsPropertyThatListsAllKnownProducts() {
        Assert.assertThat("it has a products property that starts as empty", products.getProducts().length, is(equalTo(0)));
    }
    @Test
    public void itHasAnAddProductMethodThatIntroducesANewProduct() {
        Product product = new Product("title", "description", 1.0f, 2);
        products.addProduct(product);
        Assert.assertThat("products property should contain a single product", products.getProducts().length, is(equalTo(1)));
        Assert.assertThat("products property should contain the new product", products.getProducts()[0], is(equalTo(product)));
    }
    @Test
    public void itHasATotalPropertyThatReturnsTheTotalUnitPriceOfAllKnownProducts() {
        Assert.assertThat("It is initially a zero total", products.getTotal(), is(equalTo(0.0f)));
        products.addProduct(new Product("title", "description", 10.0f, null));
        Assert.assertThat("Adding one product sets the total to 10.00", products.getTotal(), is(equalTo(10.0f)));
        products.addProduct(new Product("title", "description", 25.5f, null));
        Assert.assertThat("Adding a second product brings the total to 35.50", products.getTotal(), is(equalTo(35.5f)));
    }
}
