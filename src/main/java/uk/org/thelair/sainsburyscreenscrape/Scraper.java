package uk.org.thelair.sainsburyscreenscrape;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Scraper {
    private static final String WEB_TEST_URL = "https://jsainsburyplc.github.io/serverside-test/site/www.sainsburys.co.uk/webapp/wcs/stores/servlet/gb/groceries/berries-cherries-currants6039.html";
    private static final String PRODUCT_LISTER_ID = "productLister";
    private static final String DIV_PRODUCT_INFOS_SELECTOR = "div.product";
    private static final String PRICE_PATTERN_REGEX = "(\\d)(\\.)(\\d){2}";
    private static final String DIV_PRODUCT_INFO_SELECTOR = "div.productInfo";
    private static final String DIV_PRODUCT_NAME_AND_PROMOTIONS_SELECTOR = "div.productNameAndPromotions";
    private static final String H_3_TAG = "h3";
    private static final String A_HREF_TAG = "a[href]";
    private static final String HREF_ATTRIBUTE = "href";
    private static final String DIV_ADD_TO_TROLLEYTAB_BOX_SELECTOR = "div.addToTrolleytabBox";
    private static final String DIV_ADD_TO_TROLLEYTAB_CONTAINER_SELECTOR = "div.addToTrolleytabContainer";
    private static final String DIV_PRICING_AND_TROLLEY_OPTIONS_SELECTOR = "div.pricingAndTrolleyOptions";
    private static final String DIV_PRICE_TAB_SELECTOR = "div.priceTab";
    private static final String DIV_PRICING_SELECTOR = "div.pricing";
    private static final String P_PRICE_PER_UNIT_SELECTOR = "p.pricePerUnit";
    private static final String HTTPS_PROTOCOL_PREFIX = "https://";
    private static final String PATH_SEPARATOR = "/";
    private static final String PARENT_PATH = "..";
    private final Pattern pricePattern;
    private String targetUrl;

    public Scraper(String url) {
        this.targetUrl = url;
        pricePattern = Pattern.compile(PRICE_PATTERN_REGEX);
    }

    public static void main(String[] args) {
        Scraper scraper = new Scraper(WEB_TEST_URL);
        scraper.run();
    }

    void run() {
        Products products = new Products();
        Document document = null;
        try {
            document = capture(targetUrl);
        } catch (Exception ex) {
            System.out.println("Failed to capture URL");
        }
        if (document != null) {
            Element[] productsCaptured = scanProductLister(document);
            for (Element rawProduct : productsCaptured) {
                Product product = scrapeProduct(rawProduct);
                products.addProduct(product);
            }
            System.out.println(pojoToJson(products));
        }
    }

    String pojoToJson(Products products) {
        String result;
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            result = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(products);
        } catch (IOException ex) {
            result = "Failed to convert output to json";
        }
        return result;
    }

    Document capture(String url) throws Exception {
        return Jsoup.connect(url).get();
    }

    Element[] scanProductLister(Document document) {
        Element[] result = new Element[0];
        List<Element> products = new ArrayList<>();
        Element productLister = document.getElementById(PRODUCT_LISTER_ID);
        if (productLister != null) {
            Elements productInfos = productLister.select(DIV_PRODUCT_INFOS_SELECTOR);
            products.addAll(productInfos);
            result = products.toArray(result);
        }
        return result;
    }

    Product scrapeProduct(Element element) {
        Product result;
        Element titleElement = element.select(DIV_PRODUCT_INFO_SELECTOR)
                .select(DIV_PRODUCT_NAME_AND_PROMOTIONS_SELECTOR)
                .select(H_3_TAG)
                .select(A_HREF_TAG)
                .get(0);
        String title = titleElement.text();
        String productLink = titleElement.attr(HREF_ATTRIBUTE);
        if (!productLink.contains(HTTPS_PROTOCOL_PREFIX)) {
            productLink = mergeRelativeUrl(targetUrl, productLink);
        }
        Float perUnitPrice = 0.0f;
        Element pricingElement = element.select(DIV_ADD_TO_TROLLEYTAB_BOX_SELECTOR)
                .select(DIV_ADD_TO_TROLLEYTAB_CONTAINER_SELECTOR)
                .select(DIV_PRICING_AND_TROLLEY_OPTIONS_SELECTOR)
                .select(DIV_PRICE_TAB_SELECTOR)
                .select(DIV_PRICING_SELECTOR)
                .select(P_PRICE_PER_UNIT_SELECTOR)
                .get(0);
        Matcher priceMatcher = pricePattern.matcher(pricingElement.text());
        if (priceMatcher.find()) {
            perUnitPrice = Float.parseFloat(priceMatcher.group(0));
        }
        result = followLink(new Product(title, "", perUnitPrice, null), productLink);
        return result;
    }

    Product followLink(Product product, String productLink) {
        Document document;
        try {
            document = capture(productLink);
            ProductPage page = ProductPageFactory.supplyPage(document);
            if (page.identifyPage(document)) {
                product = page.scrapeDetails(document, product);
            }
        } catch (Exception ex) {
            System.out.println("Product url " + productLink + " cannot be contacted");
        }
        return product;
    }

    String mergeRelativeUrl(String currentPath, String relativePath) {
        String[] currentPathParts = currentPath.split(PATH_SEPARATOR);
        String[] relativePathParts = relativePath.split(PATH_SEPARATOR);
        int index = 0;
        while (relativePathParts[index].equals(PARENT_PATH)) {
            ++index;
        }
        int mergeIndex = currentPathParts.length - index - 1;
        String result = currentPathParts[0];
        for (int builderIndex = 1; builderIndex < mergeIndex; ++builderIndex) {
            result = result.concat(PATH_SEPARATOR).concat(currentPathParts[builderIndex]);
        }
        for (int builderIndex = index; builderIndex < relativePathParts.length; ++builderIndex) {
            result = result.concat(PATH_SEPARATOR).concat(relativePathParts[builderIndex]);
        }
        return result;
    }
}
