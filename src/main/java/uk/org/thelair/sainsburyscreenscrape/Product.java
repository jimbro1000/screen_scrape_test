package uk.org.thelair.sainsburyscreenscrape;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;

class Product {
    @Getter
    @Setter
    private String title;
    @Getter
    @Setter
    private String description;
    @Getter
    @Setter
    @JsonProperty("unit_price")
    @JsonSerialize(using = JsonMoneySerialiser.class)
    private Float unitPrice;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("kcal_per_100g")
    private Integer kCalPerHundredGramme;

    Product() {
        this.title = "";
        this.description = "";
        this.unitPrice = 0.0f;
        this.kCalPerHundredGramme = null;
    }

    Product(
            String title,
            String description,
            float unitPrice,
            Integer calories
    ) {
        this.title = title;
        this.description = description;
        this.unitPrice = unitPrice;
        this.kCalPerHundredGramme = calories;
    }

    Integer getKCalPerHundredGramme() {
        return kCalPerHundredGramme;
    }

    void setKCalPerHundredGramme(Integer value) {
        this.kCalPerHundredGramme = value;
    }
}
