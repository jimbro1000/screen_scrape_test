package uk.org.thelair.sainsburyscreenscrape;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

class Products {
    @JsonIgnore
    private List<Product> products;
    @Getter
    @JsonSerialize(using = JsonMoneySerialiser.class)
    private Float total;

    Products() {
        this.products = new ArrayList<>();
        this.total = 0.0f;
    }

    @JsonProperty("results")
    Product[] getProducts() {
        return products.toArray(new Product[0]);
    }

    void setResults(Product[] products) {
        Collections.addAll(this.products, products);
    }

    void addProduct(Product product) {
        products.add(product);
        total += product.getUnitPrice();
    }
}
