package uk.org.thelair.sainsburyscreenscrape;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class ProductPageWithMainPart implements ProductPage {

    private static final String INFORMATION_ID = "information";
    private static final String MAIN_PART_ID = "mainPart";
    private static final String DIV_PRODUCT_TEXT_SELECTOR = "div.productText";
    private static final String H_3_SELECTOR = "h3";
    private static final String P_TAG_SELECTOR = "p";
    private static final String DESCRIPTION = "Description";

    @Override
    public Product scrapeDetails(Document productDom, Product partialProduct) {
        if (productDom != null && partialProduct != null) {
            Element validProductInformation = productDom.getElementById(INFORMATION_ID);
            if (validProductInformation != null) {
                Elements content = validProductInformation.select(DIV_PRODUCT_TEXT_SELECTOR);
                for (int index = 0; index < content.size(); ++index) {
                    Elements headers = content.select(H_3_SELECTOR);
                    if (headers.get(0).text().equals(DESCRIPTION)) {
                        Elements descriptionParagraphs = content.select(P_TAG_SELECTOR);
                        int paragraphIndex = 0;
                        String description = "";
                        while (paragraphIndex <= descriptionParagraphs.size() && description.equals("")) {
                            description = descriptionParagraphs.get(paragraphIndex).text();
                            ++paragraphIndex;
                        }
                        partialProduct.setDescription(description);
                        break;
                    }
                }
            }
        }
        return partialProduct;
    }

    @Override
    public boolean identifyPage(Document productDom) {
        boolean result = false;
        if (productDom != null) {
            Element validProductInformation = productDom.getElementById(INFORMATION_ID);
            if (validProductInformation != null) {
                Element content = validProductInformation.getElementById(MAIN_PART_ID);
                result = (content != null);
            }
        }
        return result;
    }
}
