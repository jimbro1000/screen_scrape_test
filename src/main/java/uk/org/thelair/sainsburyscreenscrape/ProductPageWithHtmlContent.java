package uk.org.thelair.sainsburyscreenscrape;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ProductPageWithHtmlContent implements ProductPage {
    private static final String KCAL_PATTERN_REGEX = "kcal";
    private static final String KCAL_HEADER_ENERGY_REGEX = "^Energy$";
    private static final String KCAL_HEADER_ENERGYKCAL_REGEX = "^Energy kcal$";
    private static final String INFORMATION_ID = "information";
    private static final String HTMLCONTENT_TAG = "htmlcontent";
    private static final String H3_PRODUCT_DATA_ITEM_HEADER_SELECTOR = "h3.productDataItemHeader";
    private static final String DIV_PRODUCT_TEXT_SELECTOR = "div.productText";
    private static final String DESCRIPTION_TEXT = "Description";
    private static final String NUTRITION_TEXT = "Nutrition";
    private static final String P_TAG_SELECTOR = "p";
    private static final String TD_TAG_SELECTOR = "td";
    private static final String TR_TAG_SELECTOR = "tr";
    private static final String TH_TAG_SELECTOR = "th";
    private final Pattern kcalPattern;
    private final Pattern energyPattern;
    private final Pattern energyKcalPattern;

    ProductPageWithHtmlContent() {
        kcalPattern = Pattern.compile(KCAL_PATTERN_REGEX);
        energyPattern = Pattern.compile(KCAL_HEADER_ENERGY_REGEX);
        energyKcalPattern = Pattern.compile(KCAL_HEADER_ENERGYKCAL_REGEX);
    }

    @Override
    public Product scrapeDetails(Document productDom, Product partialProduct) {
        if (partialProduct != null && productDom != null) {
            Element validProductInformation = productDom.getElementById(INFORMATION_ID);
            if (validProductInformation != null) {
                Elements content = validProductInformation.getElementsByTag(HTMLCONTENT_TAG);
                if (content.size() == 1) {
                    Elements headers = content.select(H3_PRODUCT_DATA_ITEM_HEADER_SELECTOR);
                    Elements productTexts = content.select(DIV_PRODUCT_TEXT_SELECTOR);
                    int descriptionIndex = findIndexOfItem(headers, DESCRIPTION_TEXT);
                    if (descriptionIndex != -1) {
                        Elements description = productTexts.get(descriptionIndex).select(P_TAG_SELECTOR);
                        partialProduct.setDescription(description.get(0).text());
                    }
                    int nutritionIndex = findIndexOfItem(headers, NUTRITION_TEXT);
                    if (nutritionIndex != -1) {
                        partialProduct.setKCalPerHundredGramme(extractCalories(productTexts.get(nutritionIndex)));
                    }
                }
            }
        }
        return partialProduct;
    }

    @Override
    public boolean identifyPage(Document productDom) {
        boolean result = false;
        if (productDom != null) {
            Element validProductInformation = productDom.getElementById(INFORMATION_ID);
            if (validProductInformation != null) {
                Elements content = validProductInformation.getElementsByTag(HTMLCONTENT_TAG);
                result = (content.size() == 1);
            }
        }
        return result;
    }

    Integer extractCalories(Element nutrition) {
        Integer result = null;
        String kcalText = "";
        Elements tableRows = nutrition.select(TR_TAG_SELECTOR);
        for (Element tableRow : tableRows) {
            Element header = tableRow.selectFirst(TH_TAG_SELECTOR);
            Matcher matchJustEnergy = energyPattern.matcher(header.text());
            Matcher matchEnergyKcal = energyKcalPattern.matcher(header.text());
            if (matchJustEnergy.find()) {
                Elements tableElements = nutrition.select(TD_TAG_SELECTOR);
                for (Element element : tableElements) {
                    Matcher cellMatcher = kcalPattern.matcher(element.text());
                    if (cellMatcher.find()) {
                        kcalText = element.text();
                        break;
                    }
                }
                if (!kcalText.equals("")) {
                    result = Integer.parseInt(kcalText.substring(0, kcalText.length() - 4));
                    break;
                }
            } else if (matchEnergyKcal.find()) {
                Element kCalCell = tableRow.selectFirst(TD_TAG_SELECTOR);
                kcalText = kCalCell.text();
                if (!kcalText.equals("")) {
                    result = Integer.parseInt(kcalText);
                }
                break;
            }
        }
        return result;
    }
}
