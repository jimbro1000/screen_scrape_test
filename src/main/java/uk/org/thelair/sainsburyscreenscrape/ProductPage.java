package uk.org.thelair.sainsburyscreenscrape;

import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

public interface ProductPage {
    Product scrapeDetails(Document productDom, Product partialProduct);

    boolean identifyPage(Document productDom);

    default int findIndexOfItem(Elements items, String item) {
        int result = -1;
        for (int index = 0; index < items.size(); ++index) {
            if (items.get(index).text().equals(item)) {
                result = index;
                break;
            }
        }
        return result;
    }
}
