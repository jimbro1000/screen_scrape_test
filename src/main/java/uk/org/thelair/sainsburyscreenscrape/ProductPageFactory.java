package uk.org.thelair.sainsburyscreenscrape;

import org.jsoup.nodes.Document;

import java.util.ArrayList;
import java.util.List;

class ProductPageFactory {
    private static List<ProductPage> availablePages = new ArrayList<>();

    static {
        availablePages.add(new ProductPageWithHtmlContent());
        availablePages.add(new ProductPageWithMainPart());
    }

    static ProductPage supplyPage(Document document) {
        ProductPage result = null;
        for (ProductPage page : availablePages) {
            if (page.identifyPage(document)) {
                result = page;
                break;
            }
        }
        return result;
    }
}
