package uk.org.thelair.sainsburyscreenscrape;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;

public class JsonMoneySerialiser extends JsonSerializer<Float> {
    @Override
    public void serialize(Float value, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeNumber(String.format("%.2f", value));
    }
}
