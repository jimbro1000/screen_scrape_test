# README

## Sainsbury Serverside Technical Test
## (Product Web Scraper)

The code in this repository is a solution to the serverside-test exercise posed at 
[https://jsainsburyplc.github.io/serverside-test](https://jsainsburyplc.github.io/serverside-test)

The exercise is to produce a sample Java console application for scraping product data from
[https://jsainsburyplc.github.io/serverside-test/site/www.sainsburys.co.uk/webapp/wcs/stores/servlet/gb/groceries/berries-cherries-currants6039.html](https://jsainsburyplc.github.io/serverside-test/site/www.sainsburys.co.uk/webapp/wcs/stores/servlet/gb/groceries/berries-cherries-currants6039.html).

The output of the console is a brief json structure detailing each product obtained from the web page.

## Stated Requirements

  * Follow each product’s link to get the calories per 100g (in kcal) and its long description to include in the JSON.
  * Each element in the JSON results array should contain ‘title’, ‘unit_price’, ‘kcal_per_100g’ and ‘description’ keys 
  corresponding to items in the HTML.
  * Do not include cross sell items, such as the Sainsbury’s Klip Lock Storage Set.
  * If the calories are unavailable this field should be omitted.
  * If the description is spread over multiple lines, you should scrape only the first line.
  * Show unit price and total up to 2 decimal places, representing pounds and whole pence.
  * Additionally, there should be a total field which is a sum of all unit prices on the page.

## Operation

The application is run from the console (assuming the application jar is in the immediate file path):
```
java -jar sainsburyscreenscrape-1.0.0-jar-with-dependencies.jar
```

If the thin jar is used the dependency jars must exist in the class path:
```
java -jar sainsburyscreenscrape-1.0.0.jar
```

## Dependencies

The application is built using Maven and automatically imports all dependencies from Maven Central.

The project uses a minimum set of dependencies:
  * junit
  * pitest
  * jsoup
  * jackson-databind
  * lombok

## Requirements

The code requires an installation of a Java 1.8 JDK to compile, along with a Maven 3 installation.
JDK 1.8.0_161 and Maven 3.3.9 were used in the coding of the exercise.

## Build

To compile the application use `mvn clean install`

## Testing

The application is built using TDD and utilising junit to provision and execute tests.

    note: The coding and design of this exercise has followed the TDD philosophy of only building implementation 
    code that is required. The exercise states that it will be assessed on how clean, modular and extensible the
    code is but makes no attempt to describe what "extensible" is expected to be. In the absence of any such 
    requirements no code should be built to handle it.
    
# Comments
    
There is still one assumption included in the code that is not explicitly stated in the requirements - as a console
application it is assumed at some point the target url would need to be changed.

In the course of developing the solution it became apparent that the HTML on individual product pages varies
significantly prompting the creation of product page handlers. These can be extended by adding classes that 
implement ProductPage interface and registering them in the ProductPageFactory.

With fuller requirements I would expect the application to require a suitable framework to handle dependency 
injection more gracefully. Equally a dependency to handle possible command line arguments in a sane and sensible
manner. A logger should also be used to capture operation and exception details.

Further to the above assumption, the access to the github repository holding the static test pages has been accepted
as-is. In a more thorough, richer environment the pages used for testing should be mocked to enable wider testing with
idempotent results.